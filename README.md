docker network create consul

docker run -d --name consul --net=consul -p 8400:8400 -p 8500:8500 -p 8600:53/udp -h node1 progrium/consul -server -bootstrap
docker run -it -h node \
 -p 8500:8500 \
 -p 53:53/udp \
 progrium/consul \
 -server \
 -bootstrap \
 -advertise $DOCKER_IP
 
 consul:
  command: -server -bootstrap -advertise $ROUTABLE_IP
  image: progrium/consul:latest
  ports:
  - "8300:8300"
  - "8400:8400"
  - "8500:8500"
  - "8600:53/udp"



docker run -v /var/run/docker.sock:/tmp/docker.sock -h $hostname --name registrator --net=consul -d codemonauts/registrator -ip $YourInternalHostIp -ttl 600 -ttl-refresh 300 -resync 600 -cleanup consul://consul.consul:8500

registrator:
  command: consul://consul:8500
  image: progrium/registrator:latest
  links:
  - consul
  volumes:
  - "/var/run/docker.sock:/tmp/docker.sock"



gliderlabs/registrator:latest


FROM nginx:latest

ENTRYPOINT ["/bin/start.sh"]
EXPOSE 80
VOLUME /templates
ENV CONSUL_URL consul:8500

ADD start.sh /bin/start.sh
RUN rm -v /etc/nginx/conf.d/\*.conf

ADD https://github.com/hashicorp/consul-template/releases/download/v0.7.0/consul-template_0.7.0_linux_amd64.tar.gz /usr/bin/
RUN tar -C /usr/local/bin --strip-components 1 -zxf /usr/bin/consul-template_0.7.0_linux_amd64.tar.gz


{{range services}}
  {{ if .Tags.Contains "www" }}
  upstream {{.Name}} {
    least_conn;
    {{range service .Name}}
    server  {{.Address}}:{{.Port}} max_fails=3 fail_timeout=60 weight=1;
    {{else}}server 127.0.0.1:65535;{{end}}
  }
  {{end}}
{{end}}


Dockerfile:

FROM debian:jessie
MAINTAINER sala

# Устанавливаем  nginx, curl и unzip
RUN apt-get update && apt-get install curl unzip nginx -y
# Скачиваем consul-template, распаковываем, готовим папку для шаблончиков
RUN curl -L https://releases.hashicorp.com/consul-template/0.14.0/consul-template_0.14.0_linux_amd64.zip -o consul-template.zip && \
 unzip consul-template.zip -d /usr/local/bin && \
 cd /usr/local/bin && \
 chmod +x consul-template && \
 mkdir -p /etc/consul-template/templates
# Публикуем стандартные порты для http и https
EXPOSE 80
EXPOSE 443
# Добавляем шаблон, конфигурационный файл nginx и скрипт запуска
ADD templates/ /etc/consul-template/templates
ADD nginx.conf /etc/nginx/nginx.conf
ADD scripts/start.sh .
CMD ./start.sh
templates/nginx.ctmpl

{{range service "web"}}
server {
    server_name {{.ID}}.shadam.ru;
    location / {
        proxy_pass http://{{.Address}}:{{.Port}};
        proxy_connect_timeout 90;
        proxy_send_timeout 90;
        proxy_read_timeout 90;

        proxy_buffers                   8 64k;
        proxy_buffer_size               64k;
        proxy_busy_buffers_size         64k;
        proxy_temp_file_write_size      10m;

        proxy_set_header        Host            $http_host;
        #proxy_set_header       Host            $host;
        proxy_set_header        Referer         $http_referer;
        proxy_set_header        User-Agent      $http_user_agent;proxy_redirect off;
        proxy_set_header        X-Real-IP       $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto       $scheme;

        client_max_body_size    64m;
        client_body_buffer_size 1m;
    }
}

{{end}}


scripts/start.sh

#!/bin/bash

set -eo pipefail

export CONSUL_PORT=${CONSUL_PORT:-8500}
export HOST_IP=${HOST_IP:-consul}
export CONSUL=$HOST_IP:$CONSUL_PORT

echo "[nginx] booting container. CONSUL: $CONSUL."

# Try to make initial configuration every 5 seconds until successful
consul-template -once -retry 5s -consul $CONSUL -template "/etc/consul-template/templates/nginx.ctmpl:/etc/nginx/conf.d/consul-template.conf"

# Put a continual polling `confd` process into the background to watch
# for changes every 10 seconds
consul-template  -consul $CONSUL -template "/etc/consul-template/templates/nginx.ctmpl:/etc/nginx/conf.d/consul-template.conf:service nginx reload" &
echo "[nginx] consul-template is now monitoring consul for changes..."

# Start the Nginx service using the generated config
echo "[nginx] starting nginx ..."
nginx